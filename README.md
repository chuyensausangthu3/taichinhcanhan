
##**INTRODUCTION**
  Phần mềm quản lý tài chính cá nhân giúp cung cấp các công cụ giúp người dùng có thể quản lý chi tiêu qua tiền mặt hoặc tài khoản ngân hàng mà không cần sổ sách hay các phần mềm phức tạp khác. Ứng dụng hoạt động trên tất cả thiết bị điện thoại

##**BUSINESS OBJCTIVES**
  Việc quản lý chi tiêu tài chính cá nhân yêu cầu người dùng phải ghi chép chi tiêu một cách kịp thời và đầy đủ. Tuy nhiên, việc thu/chi nhiều khoản và các vay nợ ngày càng nhiều sẽ gây khó khăn trong việc ghi chép và phân loại, đồng thời việc báo cáo thu/chi trong một khoảng thời gian nào đó sẽ là một vấn đề nan giải. Vì vậy, một ứng dụng quản lý tài chính cá nhân có thể giải quyết các vấn đề tồn đọng do cách quản lý cũ lạc hậu. Hệ thống này cho phép người sử dụng ghi chi/tiêu, xử lý các yêu cầu một cách kịp thời và chính xác hơn đồng thời đưa ra các báo cáo thống kê trực quan dễ sử dụng. Một hệ thống hiệu quả sẽ giúp người dùng quản lý tiền, chi / tiêu tốt hơn.

##**EXECUTIVE SUMMARY**
  Ứng dụng tạo ra một công cụ giúp người dùng quản lý tài chính cá nhân một cách dễ dàng hơn. Các chức năng hỗ trợ bao gồm:

- *`Ghi chép và phân loại chi tiêu theo từng hạng mục thu/chi`*
- *`Theo dõi, quản lý tiền bạc thông qua biểu đồ`*
- *`Quản lý nhiều ví như: tiền mặt, ATM`*
- *`Ghi nhớ các khoản vay nợ`*

  Những hạn chế trong ứng dụng:

- *`Chưa liên kết trực tiếp với tài khoản ngân hàng`*
- *`Các chức năng như tra cứu tỉ giá, thuế TNCN`*
- *`Tự động nhắc nhở chi tiêu`*

##**SCOPE DESCRIPTION**   
**1. Includes**
![alt](https://i.imgur.com/U3PzOpz.png)
**2. Does not includes**

- *`Liên kết với các ngân hàng`*
- *`Quét hóa đơn để tự động ghi chép thu/chi`*
- *`Chuyển đổi tiền tệ tự động`*
- *`Thuế TNCN`*
- *`Tính lãi vay`*

##**RISK ASSESSMENT**
  Các rủi ro được liệt kê dưới đây được xác định trong quá trình đánh giá rủi ro sơ bộ:

- *`Các yêu cầu được thực hiện sai lệch so với dự kiến ban đầu`*
- *`Xung đột giữa các thành viên phát triển dự án`*
- *`Các lỗi được phát hiện trễ dẫn đến tốn thời gian và chi phí sửa lỗi`*
- *`Một số thư viện không có sẵn để phân bố 100% cho dự án`*


##**MEASURES OF PROJECT SUCCESS**
  Đầu tiên, lập danh sách các user story của dự án bao gồm các thành phần như ở dưới:

![alt](https://i.imgur.com/PHRp5vL.png)


  Bảng công việc tương ưng trong team cùng thời gian cho mỗi sprint như sau:

![alt](https://i.imgur.com/xcAzbot.png)

- ́**Thời gian làm việc**: *20 giờ/người/sprint*
- **Thời gian thực hiện trung bình 1 sprint**: *80 giờ*
- **Tổng thời gian thực hiện**: *400 giờ*

##**CRITICAL SUCCESS FACTORS**

- *`Phân tích đánh giá đồ án cũng như từng task công việc cụ thể. Đưa ra yêu cầu cho đồ án về chức năng và kỹ thuật.`*
- *`Năng lực của trưởng nhóm trong việc phân công công việc cũng như đốc thúc các thành viên hoàn thành tốt công việc được giao.`*
- *`Năng lực của từng thành viên trong nhóm.`*
- *`Khảo sát, đánh giá các phần mềm tương tự`*
- *`Kiểm soát thời gian của dự án.`*
- *`Lên kế hoạch xây dựng đồ án.`*
