import Utils from '../Utils/Utils'
import { RepeatType } from '../Utils/Constant'

export default class BudgetModel {
  constructor (budgetName, totalAmounts, repeatType, startDate, expenseTypes, endDate = '', budgetParentId = '', budgetId = '') {
    this.budgetId = budgetId || Utils.guid()
    this.budgetParentId = budgetParentId || budgetId
    this.budgetName = budgetName
    this.totalAmounts = +totalAmounts
    this.expenseTypes = expenseTypes
    this.amountsUsed = 0 // TODO: calculate amountsUsed in Service
    this.repeatType = repeatType
    switch (repeatType) {
      case RepeatType.Once:
        this.endDate = endDate
        break
      case RepeatType.Week:
        this.endDate.setDate(this.startDate.getDate() + 7)
        break
      case RepeatType.Month:
        this.endDate.setMonth(this.startDate.getMonth() + 1)
        break
      case RepeatType.Year:
        this.endDate.setYear(this.startDate.getYear() + 1)
        break
      default:
        break
    }
  }
}
