import Utils from '../Utils/Utils'

export default class IncomeTypeModel {
  constructor (incomeTypeName, image, incomes, incomeTypeId = '') {
    this.incomeTypeId = incomeTypeId || Utils.guid()
    this.incomeTypeName = incomeTypeName
    this.image = image
    this.incomes = incomes
  }
}
