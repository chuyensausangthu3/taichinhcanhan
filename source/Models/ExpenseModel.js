import Utils from '../Utils/Utils'

export default class ExpenseModel {
  constructor (wallet, note, money, expenseId = '') {
    this.expenseId = expenseId || Utils.guid()
    this.wallet = wallet
    this.money = +money
    this.note = note
    this.createdDate = new Date()
  }
}
