import Utils from '../Utils/Utils'

export default class HashTagModel {
  constructor (hashTag, hashTagId = '') {
    this.hashTagId = hashTagId || Utils.guid()
    this.hashTag = hashTag
  }
}
