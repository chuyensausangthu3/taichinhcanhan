import Utils from '../Utils/Utils'

export default class WalletModel {
  constructor (walletName, walletType, money, walletId = '') {
    this.walletId = walletId || Utils.guid()
    this.walletName = walletName
    this.walletType = walletType
    this.money = +money
  }
}
