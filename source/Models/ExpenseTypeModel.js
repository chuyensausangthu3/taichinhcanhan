import Utils from '../Utils/Utils'

export default class ExpenseTypeModel {
  constructor (expenseTypeName, image, expenses, expenseTypeId = '') {
    this.expenseTypeId = expenseTypeId || Utils.guid()
    this.expenseTypeName = expenseTypeName
    this.image = image
    this.expenses = expenses
  }
}
