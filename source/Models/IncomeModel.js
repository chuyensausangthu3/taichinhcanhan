import Utils from '../Utils/Utils'

export default class IncomeModel {
  constructor (wallet, note, money, incomeId = '') {
    this.incomeId = incomeId || Utils.guid()
    this.wallet = wallet
    this.money = +money
    this.note = note
    this.createdDate = new Date()
  }
}
