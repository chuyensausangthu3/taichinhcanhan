
import WalletModel from '../Models/WalletModel'
import { repository } from './Repository'

export const WalletService = {
  create (wallet) {
    console.log(JSON.stringify(wallet))
    if (repository.objects('Wallet')
      .filtered("walletName = '" + wallet.walletName + "'").length) { return }

    repository.write(() => {
      repository.create('Wallet', wallet)
    })
    console.log(repository.objects('Wallet'))
  },

  update (wallet) {
    repository.write(() => {
      repository.create('Wallet', wallet, true)
    })
    console.log(repository.objects('Wallet'))
  },

  getAll () {
    return repository.objects('Wallet')
  },

  getWallet (walletId) {
    let wallets = repository.objects('Wallet').filtered("walletId = '" + walletId + "'")
    if (wallets.length) {
      return wallets[0]
    }
  }
}
