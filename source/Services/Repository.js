import Realm from 'realm'

const WalletSchema = {
  name: 'Wallet',
  primaryKey: 'walletId',
  properties: {
    walletId: 'int',
    walletName: 'string',
    walletType: 'string',
    money: 'double'
  }
}

const IncomeSchema = {
  name: 'Income',
  primaryKey: 'incomeId',
  properties: {
    incomeId: 'int',
    wallet: 'Wallet',
    money: 'double',
    note: 'string?',
    createdDate: 'date'
  }
}

const ExpenseSchema = {
  name: 'Expense',
  primaryKey: 'expenseId',
  properties: {
    expenseId: 'int',
    wallet: 'Wallet',
    money: 'double',
    note: 'string?',
    createdDate: 'date'
  }
}

const HashtagSchema = {
  name: 'HashTag',
  primaryKey: 'hashTagId',
  properties: {
    hashTagId: 'int',
    hashTag: 'string'
  }
}

const IncomeTypeSchema = {
  name: 'IncomeType',
  primaryKey: 'incomeTypeId',
  properties: {
    incomeTypeId: 'int',
    incomeTypeName: 'string',
    image: 'string',
    incomes: 'Income[]'
  }
}

const ExpenseTypeSchema = {
  name: 'ExpenseType',
  primaryKey: 'expenseTypeId',
  properties: {
    expenseTypeId: 'int',
    expenseTypeName: 'string',
    image: 'string',
    expenses: 'Expense[]'
  }
}

const BudgetSchema = {
  name: 'Budget',
  primaryKey: 'budgetId',
  properties: {
    budgetId: 'int',
    budgetName: 'string',
    totalAmounts: 'double',
    amountsUsed: 'double',
    budgetParentId: 'int',
    repeatType: 'string',
    startDate: 'date',
    endDate: 'date',
    expenseTypes: 'ExpenseType[]'
  }
}

export const repository = new Realm({schema: [
  WalletSchema,
  IncomeSchema,
  ExpenseSchema,
  IncomeTypeSchema,
  ExpenseTypeSchema,
  BudgetSchema,
  HashtagSchema
]})
