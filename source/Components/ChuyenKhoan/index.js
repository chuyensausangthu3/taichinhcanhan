import React, { Component } from 'react'
import {
  Platform,
  StyleSheet,
  Text,
  View,
  TouchableOpacity,
  TextInput,
  KeyboardAvoidingView
} from 'react-native'
import {
  Container, Header, Content, Left, Right, Body, Title, Button, Footer, FooterTab, Fab
} from 'native-base'
import Icon from 'react-native-vector-icons/dist/Entypo'
import Icon1 from 'react-native-vector-icons/dist/MaterialIcons'
import Icon2 from 'react-native-vector-icons/dist/Ionicons'
export default class ThemTaiKhoan extends Component {
  render () {
    console.disableYellowBox = true
    return (
      <Container>
        <Header style={{backgroundColor: '#FF6699'}}>
          <Left/>
          <Body/>
          <Right/>
        </Header>
        <View style={styles.content}>
          <View style={styles.propsView}>
            <View style={styles.nametype}>
              <View style={styles.nameView}>
                <View style={styles.lbNameView}>
                  <Text style={styles.lbName}>Tài khoản chuyển đi</Text>
                </View>
                <TextInput
                  placeholderTextColor='#DDDDDD'
                  placeholder='Tên tài khoản'
                  underlineColorAndroid='transparent'
                  style={styles.name}
                >
                </TextInput>
              </View>
              <View style={styles.typeWalletView}>
                <View style={styles.lbNameView}>
                  <Text style={styles.lbName}>Tài khoản chuyển đến</Text>
                </View>
                <TextInput
                  placeholderTextColor='#DDDDDD'
                  placeholder='Tên tài khoản'
                  underlineColorAndroid='transparent'
                  style={styles.name}
                >
                </TextInput>
              </View>
            </View>
            <View style={styles.moneyView}>
              <View style={styles.money}>
                <TextInput
                  placeholderTextColor='#DDDDDD'
                  placeholder='Số tiền chuyển'
                  keyboardType='numeric'
                  underlineColorAndroid='transparent'
                  style={styles.amount}
                >
                </TextInput>
                <View style={styles.currencyView}>
                  <Text style={styles.currency}>đ</Text>
                </View>
              </View>
            </View>
            <View style={{marginTop: 10}}>
              <Button iconLeft full style={{backgroundColor: '#009966', height: 40, borderRadius: 3, marginTop: 10}}>
                <Icon1 name='save' size={26} color='#FFFFFF' />
                <Text style={{color: '#FFFFFF', fontSize: 16, fontWeight: 'bold', marginLeft: 10}}>Lưu</Text>
              </Button>
            </View>
          </View>

          <View style={styles.keyBoardView}>

          </View>
        </View>
      </Container>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: 'red'
  },
  content: {
    flex: 1,
    flexDirection: 'column',
    backgroundColor: '#DDDDDD'
  },
  propsView: {
    margin: 10
  },
  moneyView: {
    marginTop: 10
  },
  keyBoardView: {
    backgroundColor: 'transparent'
  },
  nameView: {
    height: 50,
    backgroundColor: '#FFFFFF',
    borderTopLeftRadius: 5,
    borderTopRightRadius: 5,
    flexDirection: 'row'
  },
  typeWalletView: {
    height: 50,
    marginTop: 3,
    backgroundColor: '#FFFFFF',
    borderBottomLeftRadius: 5,
    borderBottomRightRadius: 5,
    borderBottomColor: '#999999',
    borderBottomWidth: 2,
    flexDirection: 'row'
  },
  nametype: {

  },
  money: {
    height: 70,
    backgroundColor: '#FFFFFF',
    borderRadius: 5,
    borderColor: '#999999',
    borderWidth: 2,
    justifyContent: 'center',
    flexDirection: 'row'

  },
  amount: {
    flex: 4,
    textAlign: 'right',
    fontSize: 24,
    fontWeight: 'bold'
  },
  currencyView: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: '#DDDDDD',
    margin: 10
  },
  currency: {
    fontSize: 24,
    fontWeight: 'bold'
  },
  lbName: {
    fontWeight: 'bold',
    fontSize: 16
  },
  name: {
    flex: 2,
    textAlign: 'right',
    fontWeight: 'bold',
    fontSize: 16,
    marginRight: 20
  },
  lbNameView: {
    flex: 3,
    justifyContent: 'center',
    marginLeft: 10
  },
  lbTypeView: {
    flex: 1,
    justifyContent: 'center',
    paddingLeft: 10
  },
  btnIconView: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    flexDirection: 'row'
  },
  iconWallet: {
    flex: 1,
    paddingLeft: 10
  },
  icon: {
    width: 32,
    height: 32,
    borderRadius: 45,
    backgroundColor: '#333399',
    justifyContent: 'center',
    alignItems: 'center'
  },
  txtWalletView: {
    flex: 2
  },
  iconMore: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center'
  },
  txtWallet: {
    textAlign: 'right',
    fontWeight: 'bold',
    fontSize: 16
  }
})
