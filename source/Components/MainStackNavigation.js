import React from 'react'
import {StackNavigator, TabNavigator} from 'react-navigation'
import TaiKhoan from './TaiKhoan/index.js'
import ThemTaiKhoan from './ThemTaiKhoan/index.js'
import SuaTaiKhoan from './SuaTaiKhoan/index.js'
import ChuyenKhoan from './ChuyenKhoan/index.js'
import {
  TouchableOpacity, Text
} from 'react-native'
import {
  Container, Header, Content, Left, Right, Body, Title, Button
} from 'native-base'
import Icon from 'react-native-vector-icons/dist/Entypo'
import Icon1 from 'react-native-vector-icons/dist/MaterialIcons'

export const ViStack = StackNavigator({
  TaiKhoan: {
    screen: TaiKhoan,
    navigationOptions: {
      headerTitle: 'Tài khoản',
      headerTransparent: true,
      headerTintColor: '#FFFFFF'
    }
  },
  ThemTaiKhoan: {
    screen: ThemTaiKhoan,
    navigationOptions: {
      title: 'Thêm ví',
      headerTransparent: true,
      tabBarVisible: false,
      headerTintColor: '#FFFFFF'
    }
  },
  SuaTaiKhoan: {
    screen: SuaTaiKhoan,
    navigationOptions: {
      title: 'Sửa tài khoản',
      headerTransparent: true,
      tabBarVisible: false,
      headerTintColor: '#FFFFFF'
    }
  },
  ChuyenKhoan: {
    screen: ChuyenKhoan,
    navigationOptions: {
      title: 'Chuyển khoản',
      headerTransparent: true,
      tabBarVisible: false,
      headerTintColor: '#FFFFFF'
    }
  }
})

export const GiaoDichStack = StackNavigator({
  TaiKhoan: {
    screen: TaiKhoan,
    navigationOptions: {
      title: 'Tài khoản',
      headerTransparent: true,
      headerTintColor: '#FFFFFF'
    }
  },
  ThemTaiKhoan: {
    screen: ThemTaiKhoan,
    navigationOptions: {
      title: 'Thêm ví',
      headerTransparent: true,
      headerTintColor: '#FFFFFF'
    }
  }
})

export const BaoCaoStack = StackNavigator({
  TaiKhoan: {
    screen: TaiKhoan,
    navigationOptions: {
      title: 'Tài khoản',
      headerTintColor: '#FFFFFF',
      headerTransparent: true
    }
  },
  ThemTaiKhoan: {
    screen: ThemTaiKhoan,
    navigationOptions: {
      title: 'Thêm ví',
      headerTransparent: true,
      headerTintColor: '#FFFFFF'
    }
  }
})

export const NganSachStack = StackNavigator({
  TaiKhoan: {
    screen: TaiKhoan,
    navigationOptions: {
      title: 'Tài khoản',
      headerTintColor: '#FFFFFF',
      headerTransparent: true
    }
  },
  ThemTaiKhoan: {
    screen: ThemTaiKhoan,
    navigationOptions: {
      title: 'Thêm ví',
      headerTransparent: true,
      headerTintColor: '#FFFFFF'
    }
  }
})

export const AccountStack = StackNavigator({
  TaiKhoan: {
    screen: TaiKhoan,
    navigationOptions: {
      title: 'Tài khoản',
      headerTintColor: '#FFFFFF',
      headerTransparent: true
    }
  },
  ThemTaiKhoan: {
    screen: ThemTaiKhoan,
    navigationOptions: {
      title: 'Thêm ví',
      headerTransparent: true,
      headerTintColor: '#FFFFFF'
    }
  }
})

export const ThemGiaoDichStack = StackNavigator({
  TaiKhoan: {
    screen: TaiKhoan,
    navigationOptions: {
      title: 'Tài khoản',
      headerTintColor: '#FFFFFF',
      headerTransparent: true
    }
  }
})

export const Tabbar = TabNavigator({
  GiaoDich: {
    screen: GiaoDichStack
  },
  BaoCao: {
    screen: BaoCaoStack
  },
  ThemGiaoDich: {
    screen: ThemGiaoDichStack
  },
  NganSach: {
    screen: NganSachStack
  },
  Vi: {
    screen: ViStack
  }
},
{
  navigationOptions: ({ navigation }) => ({
    tabBarIcon: ({ tintColor }) => {
      const { routeName } = navigation.state
      let iconName
      let sizeIcon
      if (routeName === 'GiaoDich') {
        iconName = 'list'
        sizeIcon = 25
      } else if (routeName === 'BaoCao') {
        iconName = 'line-graph'
        sizeIcon = 25
      } else if (routeName === 'ThemGiaoDich') {
        iconName = 'circle-with-plus'
        sizeIcon = 44
        tintColor = '#00CC00'
      } else if (routeName === 'NganSach') {
        iconName = 'credit'
        sizeIcon = 25
      } else if (routeName === 'Vi') {
        iconName = 'heart'
        sizeIcon = 25
      }
      return <Icon name={iconName} size={sizeIcon} color={tintColor}/>
    },
    tabBarLabel: ({ tintColor }) => {
      const { routeName } = navigation.state
      let labelName
      if (routeName === 'GiaoDich') {
        labelName = 'Giao Dịch'
      } else if (routeName === 'BaoCao') {
        labelName = 'Báo Cáo'
      } else if (routeName === 'ThemGiaoDich') {
        labelName = ''
      } else if (routeName === 'NganSach') {
        labelName = 'Ngân Sách'
      } else if (routeName === 'Vi') {
        labelName = 'Ví'
      };
      return labelName
    }
  }),
  tabBarPosition: 'bottom',
  tabBarOptions: {
    style: {
      backgroundColor: 'transparent'
    },
    indicatorStyle: {
      backgroundColor: 'transparent'
    },
    activeTintColor: '#FF6699',
    inactiveTintColor: '#676060',
    showIcon: true,
    showLabel: true,
    upperCaseLabel: false,
    labelStyle: {
      fontSize: 13,
      fontWeight: 'bold'

    },
    tabStyle: {
      height: 80,
      padding: 0
    },
    iconStyle: {
      height: 45,
      width: 45,
      alignSefl: 'center'
    }
  },
  swipeEnabled: true
})
