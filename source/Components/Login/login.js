import React, { Component } from 'react';
import {
  Platform, StyleSheet, Text,
  View, Image, ImageBackground, TextInput, Button,
  TouchableHighlight, StatusBar
} from 'react-native';


class Login extends Component {
  handleSubmit = () => {
    const value = this._form.getValue(); // use that ref to get the form value
    console.log('value: ', value);
}
  render() {
    return (
      <View style = {styles.container}>
          <ImageBackground style= {styles.background} source = {require('../Image/background.png')}>
              <View style = {styles.container}>
                  <View style = {styles.center}>
                      <View style = {styles.form}>
                          <View style = {styles.infoUser}>
                              <TextInput style =  {styles.inputUser}
                                  // underlineColorAndroid='transparent'
                                  placeholder = "Enter your user name"
                                  placeholderTextInput = 'white'/>
                              <Image style = {styles.logoUser}
                                  source = {require('../Image/user.png')}/>
                          </View>
                          <View style = {styles.infoPassword}>
                              <TextInput style =  {styles.inputPassword}
                                  //underlineColorAndroid='transparent'
                                  placeholder = "Password"
                                  placeholderTextInput = 'white'/>
                              <Image style = {styles.logoPassword}
                                  source = {require('../Image/key.png')}/>
                          </View>
                          <View>
                              <TouchableHighlight style={styles.button} onPress={this.onPress}>
                                  <Text style={styles.buttonText}>LOGIN</Text>
                              </TouchableHighlight>
                          </View>
                          <View>
                              <Text style={styles.text}>OR</Text>
                              <TouchableHighlight style={styles.buttonSignUp} onPress={this.onPress}>
                                  <Text style={styles.buttonText}>SIGN  UP</Text>
                              </TouchableHighlight>
                          </View>

                      </View>
                  </View>
              </View>
          </ImageBackground>
      </View>
    );
  }
}


const styles = StyleSheet.create({
  container: {
    flex: 1,
    // backgroundColor: 'red',
    flexDirection: 'column',
  },
  center: {
    alignItems: 'center',
    justifyContent: 'center',
    flex: 1,
  },
  background: {
    width: '100%',
    height: '100%',
  },
  title: {
    color: '#f7c744',
    fontSize: 18,
    textAlign: 'center',
    marginTop: 5,
    opacity: 0.9,
  },
  form: {
    width: 300,
    marginTop: 220,
  },
  infoUser: {
    marginBottom: 20,
    position: 'relative',
  },
  inputUser: {
    // backgroundColor: '#eee',
    borderRadius: 5,
    height: 40,
    paddingLeft: 40,
    fontStyle: 'italic'
  },
  infoPassword: {
    marginBottom: 20,
    position: 'relative',
  },
  inputPassword: {
    // backgroundColor: '#eee',
    borderRadius: 5,
    height: 40,
    paddingLeft: 40,
    fontStyle: 'italic'
  },
  logoUser: {
    position: 'absolute',
    top: 10,
    left: 10,
    width: 20,
    height: 20
  },
  logoPassword: {
    position: 'absolute',
    top: 10,
    left: 10,
    width: 20,
    height: 20
  },
  button: {
    backgroundColor: '#12c48b',
    borderRadius: 5,
    height: 40,
    width: 300,
    justifyContent: 'center'
  },
  buttonText: {
    textAlign: 'center',
    color: '#fff'
  },
  text: {
    marginTop: 20,
    textAlign: 'center',
  },
  buttonSignUp: {
    backgroundColor: '#fe5a61',
    borderRadius: 5,
    height: 40,
    width: 300,
    justifyContent: 'center',
    marginTop: 20,
  }
  // buttonFB:{
  //   backgroundColor: '#4267b2',
  //   borderRadius: 5,
  //   height: 40,
  //   width: 100,
  //   justifyContent: 'center',
  //   marginTop: 30,
  //   left: 30
  // },
  // logoFacebook:{
  //   position: 'absolute',
  //   top: 10,
  //   left: 40,
  //   width: 20,
  //   height: 20,
  //   borderRadius: 10,
  // },
  // buttonGG:{
  //   backgroundColor: '#eee',
  //   borderRadius: 5,
  //   height: 40,
  //   width: 100,
  //   justifyContent: 'center',
  //   marginTop: 30,
  //   right: 30
  // },
  // logoGoogle:{
  //   position: 'absolute',
  //   top: 10,
  //   left: 40,
  //   width: 20,
  //   height: 20,
  //   borderRadius: 10,
  // },
  // textForget:{
  //   marginTop: 90,
  //   textAlign: 'center',
  // }
});

export default Login;
