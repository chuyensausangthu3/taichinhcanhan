import React, { Component } from 'react'
import {
  Platform,
  StyleSheet,
  Text,
  View,
  TouchableOpacity,
  TextInput,
  KeyboardAvoidingView
} from 'react-native'
import {
  Container, Header, Content, Left, Right, Body, Title, Button, Footer, FooterTab, Fab
} from 'native-base'
import Icon from 'react-native-vector-icons/dist/Entypo'
import Icon1 from 'react-native-vector-icons/dist/MaterialIcons'
import Icon2 from 'react-native-vector-icons/dist/FontAwesome'
export default class ThemTaiKhoan extends Component {
  render () {
    console.disableYellowBox = true
    return (
      <Container>
        <Header style={{backgroundColor: '#FF6699'}}>
          <Left/>
          <Body/>
          <Right>
            <TouchableOpacity onPress={() => { this.props.navigation.navigate('ThemTaiKhoan') }}>
              <Text style={{fontWeight: 'bold', fontSize: 16, color: '#FFFFFF', paddingRight: 10}}>Lưu</Text>
            </TouchableOpacity>
          </Right>
        </Header>
        <View style={styles.content}>
        </View>
      </Container>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: 'red'
  },
  content: {
    flex: 1,
    flexDirection: 'column',
    backgroundColor: '#DDDDDD'
  }
})
