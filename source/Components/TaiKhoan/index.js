import React, { Component } from 'react'
import {
  Text,
  View,
  TouchableOpacity,
  FlatList
} from 'react-native'
import {
  Container, Header, Content, Left, Right, Body, Title, Button, Footer, FooterTab
} from 'native-base'

import styles from './styles.js'

import Icon from 'react-native-vector-icons/dist/Entypo'
import Icon1 from 'react-native-vector-icons/dist/MaterialIcons'
import { WalletService } from '../../Services/WalletService.js'

export default class TaiKhoan extends Component {
  constructor (props) {
    super(props)
    this.state = {
      wallets: WalletService.getAll()
    }
  }
  render () {
    console.disableYellowBox = true
    return (
      <Container>
        <Header style={{backgroundColor: '#FF6699'}}>
          <Left/>
          <Body/>
          <Right>
            <TouchableOpacity onPress={() => { this.props.navigation.navigate('ThemTaiKhoan') }}>
              <Icon1 name='add-circle-outline' size={26} color='#FFFFFF' style={{paddingRight: 10}}/>
            </TouchableOpacity>
          </Right>
        </Header>
        <Content>
          <FlatList style={{margin: 10}}
            data={this.state.wallets}
            renderItem={({item}) => (
              <TouchableOpacity onPress={() => { this.props.navigation.navigate('SuaTaiKhoan', {walletId: item.walletId}) }}>
                <View style= {{flexDirection: 'row', borderBottomWidth: 1, borderColor: '#e5e5e5'}}>
                  <Icon transparent name='credit-card' size={35} color='#2db84c' style = {{padding: 5}}/>
                  <View style={{paddingLeft: 10}}>
                    <Text style={{paddingTop: 5, fontWeight: 'bold'}}>{item.walletName}</Text>
                    <Text style={{paddingBottom: 5, fontWeight: 'bold'}}>{item.money} đ</Text>
                  </View>
                </View>
              </TouchableOpacity>
            )}/>

        </Content>

      </Container>
    )
  }
}
