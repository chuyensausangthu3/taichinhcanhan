import React, { Component } from 'react'
import {
  Platform,
  StyleSheet,
  Text,
  View,
  TouchableOpacity,
  TextInput,
  KeyboardAvoidingView
} from 'react-native'
import {
  Container, Header, Content, Left, Right, Body, Title, Button, Footer, FooterTab, Fab, Picker, Form, Icon
} from 'native-base'
import Icon3 from 'react-native-vector-icons/dist/Entypo'
import Icon1 from 'react-native-vector-icons/dist/MaterialIcons'
import Icon2 from 'react-native-vector-icons/dist/Ionicons'
import WalletModel from '../../Models/WalletModel'
import { WalletService } from '../../Services/WalletService'
export default class ThemTaiKhoan extends Component {
  constructor (props) {
    super(props)
    this.state = {
      walletName: 'Ten tai khoan',
      walletType: 'Tien mat',
      money: 0,
      selected: "key0",
      nameIcon: 'wallet'
    }
  }

  onPressSaveButton () {
    const {walletName, walletType, money} = this.state
    let wallet = new WalletModel(walletName, walletType, money)
    WalletService.create(wallet)

    this.props.navigation.navigate('TaiKhoan')
  }

  onValueChange(value: string) {
    this.setState({
      selected: value,
    });
  }




  render () {
    console.disableYellowBox = true
    return (
      <Container>
        <Header style={{backgroundColor: '#FF6699'}}>
          <Left/>
          <Body/>
          <Right>
            <TouchableOpacity onPress={() => { this.props.navigation.navigate('ThemTaiKhoan') }}>
              <Text style={{fontWeight: 'bold', fontSize: 16, color: '#FFFFFF', paddingRight: 10}}>Lưu</Text>
            </TouchableOpacity>
          </Right>
        </Header>
        <View style={styles.content}>
          <View style={styles.propsView}>
            <View style={styles.nametype}>
              <View style={styles.nameView}>
                <View style={styles.lbNameView}>
                  <Text style={styles.lbName}>Tên</Text>
                </View>
                <TextInput
                  placeholderTextColor='#DDDDDD'
                  placeholder='Tên tài khoản'
                  underlineColorAndroid='transparent'
                  style={styles.name}

                  onChangeText={(walletName) => this.setState({walletName})}
                >
                </TextInput>
              </View>
              <View style={styles.typeWalletView}>
                <View style={styles.lbTypeView}>
                  <Text style={styles.lbName}>Thuộc loại</Text>
                </View>
                <View style={styles.btnIconView}>
                  <View style={styles.iconWallet}>
                    <View style={[styles.icon, {backgroundColor: (this.state.selected === 'key0') ? '#5964a1' : (this.state.selected === 'key1') ? '#cd512f' : (this.state.selected === 'key2') ? '#047b9a' : (this.state.selected === 'key3') ? '#3b77b5' : '#e39b35'}]}>
                      <Icon3 name= {this.state.selected === 'key0' ? 'wallet' : (this.state.selected === 'key1' ? 'home' : (this.state.selected === 'key2' ? 'credit-card' :  (this.state.selected === 'key3' ? 'bar-graph' : 'briefcase')))} size={20} color='#FFFFFF'
                      />
                    </View>
                  </View>
                  <View style={styles.txtWalletView}>
                    <Form>
                      <Picker
                        mode="dropdown"
                        iosIcon={<Icon name="ios-arrow-down-outline" />}
                        style={{ width: undefined }}
                        selectedValue={this.state.selected}
                        onValueChange={this.onValueChange.bind(this)}
                      >
                        <Picker.Item label="Tiền mặt" value="key0" />
                        <Picker.Item label="Tài khoản ngân hàng" value="key1" />
                        <Picker.Item label="Thẻ tín dụng" value="key2" />
                        <Picker.Item label="Tài khoản đầu tư" value="key3" />
                        <Picker.Item label="Tài khoản tiết kiệm" value="key4" />
                      </Picker>
                    </Form>
                  </View>
                </View>
              </View>
            </View>

            <View style={styles.moneyView}>
              <View style={styles.money}>
                <TextInput
                  placeholderTextColor='#DDDDDD'
                  placeholder='Số dư ban đầu'
                  keyboardType='numeric'
                  underlineColorAndroid='transparent'
                  style={styles.amount}
                  onChangeText={(money) => this.setState({money})}

                >
                </TextInput>
                <View style={styles.currencyView}>
                  <Text style={styles.currency}>đ</Text>
                </View>
              </View>
            </View>
            <View style={{marginTop: 10}}>
              <Button iconLeft full style={{backgroundColor: '#009966', height: 40, borderRadius: 3, marginTop: 10}}
                onPress={this.onPressSaveButton.bind(this)}>
                <Icon1 name='save' size={26} color='#FFFFFF' />
                <Text style={{color: '#FFFFFF', fontSize: 16, fontWeight: 'bold', marginLeft: 10}}>Lưu</Text>
              </Button>
            </View>
          </View>

          <View style={styles.keyBoardView}>

          </View>
        </View>
      </Container>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: 'red'
  },
  content: {
    flex: 1,
    flexDirection: 'column',
    backgroundColor: '#DDDDDD'
  },
  propsView: {
    margin: 10
  },
  moneyView: {
    marginTop: 10
  },
  keyBoardView: {
    backgroundColor: 'transparent'
  },
  nameView: {
    height: 50,
    backgroundColor: '#FFFFFF',
    borderTopLeftRadius: 5,
    borderTopRightRadius: 5,
    flexDirection: 'row'
  },
  typeWalletView: {
    height: 50,
    marginTop: 3,
    backgroundColor: '#FFFFFF',
    borderBottomLeftRadius: 5,
    borderBottomRightRadius: 5,
    borderBottomColor: '#999999',
    borderBottomWidth: 2,
    flexDirection: 'row'
  },
  nametype: {

  },
  money: {
    height: 70,
    backgroundColor: '#FFFFFF',
    borderRadius: 5,
    borderColor: '#999999',
    borderWidth: 2,
    justifyContent: 'center',
    flexDirection: 'row'

  },
  amount: {
    flex: 4,
    textAlign: 'right',
    fontSize: 24,
    fontWeight: 'bold'
  },
  currencyView: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: '#DDDDDD',
    margin: 10
  },
  currency: {
    fontSize: 24,
    fontWeight: 'bold'
  },
  lbName: {
    fontWeight: 'bold',
    fontSize: 16
  },
  name: {
    flex: 2,
    textAlign: 'right',
    fontWeight: 'bold',
    fontSize: 16,
    marginRight: 20
  },
  lbNameView: {
    flex: 3,
    justifyContent: 'center',
    marginLeft: 10
  },
  lbTypeView: {
    flex: 1,
    justifyContent: 'center',
    paddingLeft: 10,
  },
  btnIconView: {
    flex: 3,
    justifyContent: 'center',
    alignItems: 'center',
    flexDirection: 'row'
  },
  iconWallet: {
    flex: 1,
    paddingLeft: 10
  },
  icon: {
    width: 32,
    height: 32,
    borderRadius: 45,
    marginLeft: 20,
    justifyContent: 'center',
    alignItems: 'center'
  },
  txtWalletView: {
    flex: 4,
  },
  iconMore: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center'
  },
  txtWallet: {
    textAlign: 'right',
    fontWeight: 'bold',
    fontSize: 16
  }
})
