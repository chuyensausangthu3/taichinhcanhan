import { AppRegistry } from 'react-native'
import React, { Component } from 'react'

import {Tabbar} from './source/Components/MainStackNavigation.js'
import TaiKhoan from './source/Components/TaiKhoan/index.js'

AppRegistry.registerComponent('QLTC', () => Tabbar)
